/* Delete user from mysql */
DROP USER IF EXISTS 'admin'@'localhost';
DROP USER IF EXISTS 'gerente'@'localhost';
DROP USER IF EXISTS 'viajero'@'localhost';
DROP USER IF EXISTS 'camarero'@'localhost';
DROP USER IF EXISTS 'piloto'@'localhost';
DROP USER IF EXISTS 'mecanico'@'localhost';
DROP USER IF EXISTS 'vigilante'@'localhost';
DROP USER IF EXISTS 'limpieza'@'localhost';

/* Delete view tables */
DROP VIEW IF EXISTS ver_camarero;
DROP VIEW IF EXISTS ver_piloto;
DROP VIEW IF EXISTS ver_mecanico;
DROP VIEW IF EXISTS ver_vigilante;
DROP VIEW IF EXISTS ver_limpieza;

/* Create user admin. This user can change all database and create, or modify, other users*/
CREATE USER IF NOT EXISTS 'admin'@'localhost' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'localhost';

/* Create user gerente. This user can create and modify datas but not structure and other users*/
CREATE USER IF NOT EXISTS 'gerente'@'localhost' IDENTIFIED BY 'gerente';
GRANT DELETE, INSERT, SELECT ON aeropuerto_antigua.* TO 'gerente'@'localhost';

/* Create user viajero. This user can view his table, if there were more users, we should create view tables */
CREATE USER IF NOT EXISTS 'viajero'@'localhost' IDENTIFIED BY 'viajero';
GRANT SELECT ON aeropuerto_antigua.VIAJERO TO 'viajero'@'localhost';

/* Create user camarero. This user can view AEROPUERTO table and view  his column in TRABAJADOR table */
CREATE USER IF NOT EXISTS 'camarero'@'localhost' IDENTIFIED BY 'camarero';
GRANT SELECT ON aeropuerto_antigua.AEROPUERTO TO 'camarero'@'localhost';
CREATE VIEW ver_camarero AS SELECT * FROM aeropuerto_antigua.TRABAJADOR WHERE Puesto_de_trabajo = 'Camarero';

/* Create user piloto. This user can view TRABAJADOR,EROPUERTO, AEROLINEA, AVION and tiene table and view  his column in TRABAJADOR table */
CREATE USER IF NOT EXISTS 'piloto'@'localhost' IDENTIFIED BY 'piloto';
GRANT SELECT ON aeropuerto_antigua.TRABAJADOR TO 'piloto'@'localhost';
GRANT SELECT ON aeropuerto_antigua.AEROPUERTO TO 'piloto'@'localhost';
GRANT SELECT ON aeropuerto_antigua.AEROLINEA TO 'piloto'@'localhost';
GRANT SELECT ON aeropuerto_antigua.AVION TO 'piloto'@'localhost';
GRANT SELECT ON aeropuerto_antigua.tiene TO 'piloto'@'localhost';
CREATE VIEW ver_piloto AS SELECT * FROM aeropuerto_antigua.TRABAJADOR WHERE Puesto_de_trabajo = 'Piloto';

/* Create user mecanico. This user can view TRABAJADOR and EROPUERTO table and view  his column in TRABAJADOR table */
CREATE USER IF NOT EXISTS 'mecanico'@'localhost' IDENTIFIED BY 'mecanico';
GRANT SELECT ON aeropuerto_antigua.TRABAJADOR TO 'mecanico'@'localhost';
GRANT SELECT ON aeropuerto_antigua.AEROPUERTO TO 'mecanico'@'localhost';
CREATE VIEW ver_mecanico AS SELECT * FROM aeropuerto_antigua.TRABAJADOR WHERE Puesto_de_trabajo = 'Mecanico';

/* Create user vigilante. This user can view TRABAJADOR and EROPUERTO table and view  his column in TRABAJADOR table */
CREATE USER IF NOT EXISTS 'vigilante'@'localhost' IDENTIFIED BY 'vigilante';
GRANT SELECT ON aeropuerto_antigua.TRABAJADOR TO 'vigilante'@'localhost';
GRANT SELECT ON aeropuerto_antigua.AEROPUERTO TO 'vigilante'@'localhost';
CREATE VIEW ver_vigilante AS SELECT * FROM aeropuerto_antigua.TRABAJADOR WHERE Puesto_de_trabajo = 'Vigilante';

/* Create user limpieza. This user can view TRABAJADOR and EROPUERTO tiene table and view  his column in TRABAJADOR table */
CREATE USER IF NOT EXISTS 'limpieza'@'localhost' IDENTIFIED BY 'limpieza';
GRANT SELECT ON aeropuerto_antigua.TRABAJADOR TO 'limpieza'@'localhost';
GRANT SELECT ON aeropuerto_antigua.AEROPUERTO TO 'limpieza'@'localhost';
CREATE VIEW ver_limpieza AS SELECT * FROM aeropuerto_antigua.TRABAJADOR WHERE Puesto_de_trabajo = 'Limpieza';
