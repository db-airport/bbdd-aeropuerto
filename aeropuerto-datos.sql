/*                          Pasaporte     Nombre       Apellido                                */
INSERT INTO PERSONA VALUES('ABC000123',   'Pepe',      'Pérez'),
                          ('EDF000153',   'Rodrigo',   'Sánchez'),
                          ('BDC080123',   'Victor',    'Gonzalez'),
                          ('LTD008923',   'Luis',      'Sierra'),
                          ('EYF008153',   'Martina',   'Fernandez'),
                          ('QTC000014',   'Macarena',  'Rodriguez'),
                          ('KLC180183',   'Alberto',   'Diaz'),
                          ('HPF072653',   'José',      'Moreno'),
                          ('BBC189003',   'Osman',     'Dermir'),
                          ('AAA012333',   'Marta',     'Scheck');


/*                       PasaportePERSONA   Turno            NombrePERSONA  ApellidoPERSONA    */
INSERT INTO turno VALUES('ABC000123',       'Tarde',         'Pepe',        'Pérez'),
                        ('EDF000153',       'Mañana,Tarde',  'Rodrigo',     'Sánchez'),
                        ('BDC080123',       'Mañana',        'Victor',      'Gonzalez'),
                        ('BBC189003',       'Noche',         'Osman',       'Dermir'),
                        ('AAA012333',       'Noche,Mañana',  'Marta',       'Scheck');

/*                          CodigoOACI  CodigoOACI_contenedor_AEROLINEA   Nombre               */
INSERT INTO AEROLINEA VALUES('IBE',     NULL,                             'IBERIA'),
                            ('AAL',     NULL,                             'AMERICAN AIRLINES'),
                            ('UAE',     NULL,                             'EMIRATES'),
                            ('IBS',     'IBE',                            'IBERIA'),
                            ('DLH',     NULL,                             'LUFTHANSA'),
                            ('CLH',     'DLH',                            'LUFTHANSA');

/*                       Matricula  Modelo       CodigoOACI_AEROLINEA                         */
INSERT INTO AVION VALUES('ECABR',   'A319-100',  'IBE'),
                        ('N1863',   'B777-200',  'AAL'),
                        ('A6JKL',   'A380',      'UAE'),
                        ('N8931',   'A330-300',  'IBE'),
                        ('DKART',   'A380',      'DLH');

/*                        Numero_de_asiento  Tipo         MatriculaAVION                      */
INSERT INTO ASIENTO VALUES('18F',            'Bussines',  'ECABR'),
                          ('33D',            'Turist',    'N1863'),
                          ('10A',            'First',     'A6JKL'),
                          ('2B',             'First',     'N8931'),
                          ('55C',            'Turist',    'DKART');

/*                         PasaportePERSONA  Numero_asiento_AVION  Precio  Nacionalidad   NombrePERSONA  ApellidoPERSONA*/
INSERT INTO VIAJERO VALUES('LTD008923',      '18F',                160,    'Española',    'Luis',        'Sierra'),
                          ('EYF008153',      '33D',                800,    'Canadiense',  'Martina',     'Fernandez'),
                          ('QTC000014',      '10A',                2200,   'Colombiana',  'Macarena',    'Rodriguez'),
                          ('KLC180183',      '2B',                 2200,   'Turco',       'Alberto',     'Diaz'),
                          ('HPF072653',      '55C',                600,    'Alemán',      'José',        'Moreno');

/*                            CodigoOACI  Nombre                     Ubicacion   Terminal     */
INSERT INTO AEROPUERTO VALUES('LEMD',     'Adolfo Suárez',           'España',   'T1'),
                             ('KMIA',     'Internacional de Miami',  'EEUU',     'T3'),
                             ('KJFK',     'John F. Kennedy',         'EEUU',     'T8'),
                             ('LEMD',     'Adolfo Suárez',           'España',   'T3'),
                             ('LFPG',     'Paris Orly',              'Francia',  'T1');

/*                            PasaportePERSONA  CodigoOACI_AEROPUERTO  TerminalAEROPUERTO  Turno             Puesto_de_trabajo  NombrePERSONA  ApellidoPERSONA*/
INSERT INTO TRABAJADOR VALUES('ABC000123',      'LEMD',                'T3',               'Tarde',          'Mécanico',        'Pepe',        'Pérez'),
                             ('EDF000153',      'KMIA',                'T3',               'Mañana',         'Limpieza',        'Rodrigo',     'Sánchez'),
                             ('BDC080123',      'KJFK',                'T8',               'Mañana,Tarde',   'Camarero',        'Victor',      'Gonzalez'),
                             ('BBC189003',      'LEMD',                'T3',               'Noche',          'Piloto',          'Osman',       'Dermir'),
                             ('AAA012333',      'LFPG',                'T1',               'Mañana,Noche',   'Vigilante',       'Marta',       'Scheck');

/*                       Numero_vuelo  CodigoOACI_AEROLINEA  Numero_de_asiento_ASIENTO  Matricula_AVION */
INSERT INTO VUELO VALUES('IB 530',     'LEMD',               '18F',                     'ECABR'),
                        ('AA 740',     'KMIA',               '33D',                     'N1863'),
                        ('EK 370',     'KJFJ',               '10A',                     'A6JKL'),
                        ('IB 6252',    'LEMD',               '2B',                      'N8931'),
                        ('EK 385',     'LFPG',               '55C',                     'DKART');

/*                       Numero_vuelo_VUELO  TerminalAEROPUERTO  CodigoOACI_AEROPUERTO  Tipo*/
INSERT INTO tiene VALUES('IB 530',           'T1',              'LEMD',                 'Origen'),
                        ('AA 740',           'T3',              'KMIA',                 'Destino'),
                        ('EK 370',           'T8',              'KJFK',                 'Origen');


/* SHOW TABLES  */
SELECT * FROM PERSONA;
SELECT * FROM turno;
SELECT * FROM AEROLINEA;
SELECT * FROM AVION;
SELECT * FROM ASIENTO;
SELECT * FROM VIAJERO;
SELECT * FROM AEROPUERTO;
SELECT * FROM TRABAJADOR;
SELECT * FROM VUELO;
SELECT * FROM tiene;
/* END SHOW TABLES */
